export default {
  name: 'post',
  type: 'document',
  title: 'Post',
  fields: [
    {
      name: 'caption',
      type: 'string',
      title: 'Name'
    },
    {
      name: 'image',
      title: 'image',
      type: 'file',
      options: {
        hotspot: true
      }
    },
    {
      name: 'postedBy',
      title: 'PostedBy',
      type: 'postedBy'
    },
    {
      name: 'likes',
      title: 'Likes',
      type: 'array',
      of: [
        {
          type: 'reference',
          to: [{ type: 'user' }]
        }
      ]
    },
    {
      name: 'comments',
      title: 'Comments',
      type: 'array',
      of: [{ type: 'comment' }]
    },
    {
      name: 'category',
      title: 'Category',
      type: 'string'
    }
  ]
}
