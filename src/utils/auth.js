import jwtdecode from 'jwt-decode'
import { client } from '../a.sanity'

export const createOrGetUser = async (response, addUser) => {
  const decoded = jwtdecode(response.credential)
  const { name, sub, picture } = decoded
  const user = {
    _id: sub,
    _type: 'user',
    userName: name,
    image: picture
  }
  client
    .createIfNotExists(user)
    .then(() => console.log('success'))
    .catch((err) => console.log(err))
    .then(() => addUser(user))

  // axios.post(`${BASE_URL}/api/auth`, user)
}
