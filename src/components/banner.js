/* eslint-disable prettier/prettier */
import React from 'react'
import styled from 'styled-components'
import Arrow1 from '../images/arrow1.svg'
import UploadPic from '../images/upload.svg'

const Wrapper = styled.div`
  width: 90%;
  height: 600px;
  background-color: #fff;
  /* padding-bottom: 55px; */
  margin-top: 70px;
  margin-left: 60px;
  margin-right: 25px;
  border-radius: 20px;
  display: grid;
  grid-template-columns: repeat(10, 1fr);
  grid-template-columns: repeat(10, 1fr);
  line-height: 1.3;
  position: relative;
`
const LeftBanner = styled.div`
  grid-column-start: 1;
  grid-column-end: 7;
  grid-row-start: 1;
  grid-row-end: 11;
  background-color: #ccff33;
`
const RightBanner = styled.div`
  grid-column-start: 7;
  grid-column-end: 11;
  grid-row-start: 1;
  grid-row-end: 11;
  background-color: #2b2d42;
`
const Text = styled.p`
  font-size: 55px;
  padding-left: 70px;
  padding-right: 260px;
  font-weight: 700;
  font-family: 'Nunito';
  img {
    width: 60px;
    height: 60px;
    color: #4cc9f0;
  }
`
const MiniText = styled.p`
  padding-left: 70px;
  padding-bottom: 30px;
  padding-right: 260px;
  font-family: 'Nunito';
`
const Upload = styled.div`
  background-color: #83c5be;
  padding: 30px;
  margin-top: 20px;
  border-radius: 10px;
  width: 350px;
  height: auto;
  position: absolute;
  top: 60px;
  left: 50%;
`
const Title = styled.div`
  margin-bottom: 15px;
  font-size: 22px;
  font-weight: 600;
  font-family: 'Nunito';
`
const Area = styled.div`
  background-color: #e5e5e5;
  padding: 30px;
  border: 1px dashed #a5a58d;
  border-radius: 10px;
`
const Drop = styled.p`
  padding: 0;
  font-size: 28px;
  font-family: 'Nunito';
  font-weight: 100;
  color: #474448;
  span {
    color: #3a0ca3;
    font-weight: 500;
  }
`
const Size = styled.p`
  font-size: 16px;
  color: #474448;
  font-family: 'Nunito';
`
const CTA = styled.button`
  margin-top: 30px;
  padding: 15px;
  background-color: #d62828;
  color: #fff;
  text-align: center;
  border: none;
  border-radius: 10px;
  font-family: 'Nunito';
  margin-left: 30%;
  cursor: pointer;
`
const UploadImage = styled.div`
  display: flex;
  justify-content: center;
  img {
    width: 65px;
    height: 65px;
  }
`

export default function banner() {
  return (
    <Wrapper style={{ borderRadius: '7px' }}>
      <LeftBanner>
        <Text>
          Share your daily lifestyle activities{' '}
          <img src={Arrow1} alt="Arrow" style={{ color: '#4cc9f0' }} />
        </Text>
        <MiniText>
          is the world first live system for sharing and show casing your photos and toughts
        </MiniText>
      </LeftBanner>
      <RightBanner></RightBanner>
      <Upload>
        <Title>Upload</Title>
        <Area>
          <Drop>
            Drop your files <span>here</span>
          </Drop>
          <Size>JPG or PNG Max File Size: 5MB</Size>
          <UploadImage>
            <img src={UploadPic} alt="Upload-image" />
          </UploadImage>
        </Area>
        <CTA>Publish Now</CTA>
      </Upload>
    </Wrapper>
  )
}
