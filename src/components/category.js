/* eslint-disable prettier/prettier */
import React from 'react'
import styled from 'styled-components'
import Entertainment1 from '../images/entertainment.jpg'
import Boxing1 from '../images/boxing.jpg'
import Nature1 from '../images/nature.jpg'
import Cat1 from '../images/cat.jpg'
import Fashion from '../images/fashion.jpg'
import Crypto from '../images/crypto.jpg'

const Wrapper = styled.div`
  background-color: #f8f9fa;
  width: 90%;
  height: auto;
  margin-left: 60px;
  margin-right: 25px;
  margin-top: 79px;
  border-radius: 7px;
  padding-bottom: 50px;
`
const Title = styled.h3`
  text-transform: uppercase;
  text-align: center;
  padding-top: 35px;
  font-family: 'Nunito';
  font-weight: 900;
`

const Subtitle = styled.div`
  padding-top: 10px;
  display: flex;
  justify-content: center;
  align-items: center;
  font-size: 18px;
  color: #1b263b;
`
const IspDiv = styled.div`
  display: flex;
  justify-content: center;
`
const TitleTiny = styled.p`
  font-family: 'Nunito';
  font-weight: 300;
  font-size: 16px;
  line-height: 24px;
  width: 50vw;
  text-align: center;
`
const Navlink = styled.h4`
  margin-left: 30px;
  cursor: pointer;
  font-family: 'Nunito';
  padding: 6px 9px;
  font-weight: 400;
  transition: all 0.1s linear;
  &:hover {
    // background-color: #1d3557;
    background-color: ${({ theme }) => theme.colors.primary};
    color: #fff;

    border-radius: 5px;
  }
`
const Gallery = styled.div`
  width: 80%;
  margin: 2rem auto;
  display: grid;
  grid-template-columns: repeat(8, 1fr);
  grid-template-rows: repeat(6, 4vw);
  grid-gap: 2.4rem;
  .category-name {
    font-family: 'Nunito';
    font-weight: 300;
  }
  /* img{
    width: 50px;
    height: 50px;
  } */
`
const First = styled.div`
  grid-column-start: 1;
  grid-column-end: 3;
  grid-row-start: 1;
  grid-row-end: 3;
  position: relative;
`
const Second = styled.div`
  grid-column-start: 3;
  grid-column-end: 5;
  grid-row-start: 1;
  grid-row-end: 3;
  position: relative;
`
const Third = styled.div`
  grid-column-start: 5;
  grid-column-end: 9;
  grid-row-start: 1;
  grid-row-end: 5;
  position: relative;
`
const Fourth = styled.div`
  grid-column-start: 1;
  grid-column-end: 5;
  grid-row-start: 3;
  grid-row-end: 5;
  position: relative;
`
const Fifth = styled.div`
  grid-column-start: 1;
  grid-column-end: 5;
  grid-row-start: 5;
  grid-row-end: 7;
  position: relative;
`
const Sixth = styled.div`
  grid-column-start: 5;
  grid-column-end: 9;
  grid-row-start: 5;
  grid-row-end: 7;
  position: relative;
`
const Text = styled.p`
  color: ${({ theme }) => theme.colors.primary};
  font-family: 'Nunito';
  font-weight: 600;
  background: rgba(0, 0, 0, 0.3);
  height: 100%;
  width: 100%;
  position: absolute;
  display: flex;
  align-items: center;
  justify-content: center;
  color: #fff;
  top: -16px;
  left: 0;
  bottom: 0;
  border-radius: 7px;
  font-size: 17px;
`

export default function category() {
  return (
    <Wrapper>
      <Title>View by category</Title>
      <IspDiv>
        <TitleTiny>
          inspirational photos, illustrations, and graphic elements from the world’s best
          photographers. Want more fun? Browse our search results...
        </TitleTiny>
      </IspDiv>
      <Subtitle>
        <Navlink>Entertainment</Navlink>
        <Navlink>Nature</Navlink>
        <Navlink>Sports</Navlink>
        <Navlink>News</Navlink>
        <Navlink>Fashion</Navlink>
        <Navlink>Animals</Navlink>
        <Navlink>Art</Navlink>
      </Subtitle>
      <Gallery>
        <First>
          <img
            src={Entertainment1}
            alt="entertainment"
            style={{ width: '100%', height: '100%', objectFit: 'cover', borderRadius: '7px' }}
          />
          {/*   <p className="category-name" style={{ textAlign: 'center' }}>
            Entertainment
          </p> */}
          <Text>Entertainment</Text>
        </First>
        <Second>
          <img
            src={Boxing1}
            alt="boxing"
            style={{ width: '100%', height: '100%', objectFit: 'cover', borderRadius: '7px' }}
          />
          <Text>Boxing Game</Text>
        </Second>
        <Third>
          <img
            src={Nature1}
            alt="nature"
            style={{ width: '100%', height: '100%', objectFit: 'cover', borderRadius: '7px' }}
          />
          <Text>Nature walk</Text>
        </Third>
        <Fourth>
          <img
            src={Cat1}
            alt="cat"
            style={{ width: '100%', height: '100%', objectFit: 'cover', borderRadius: '7px' }}
          />
          <Text>The finest cat</Text>
        </Fourth>
        <Fifth>
          <img
            src={Fashion}
            alt="cat"
            style={{ width: '100%', height: '100%', objectFit: 'cover', borderRadius: '7px' }}
          />
          <Text>Modern Fashion</Text>
        </Fifth>
        <Sixth>
          <img
            src={Crypto}
            alt="cat"
            style={{ width: '100%', height: '100%', objectFit: 'cover', borderRadius: '7px' }}
          />
          <Text> The Crypto trading and life</Text>
        </Sixth>
      </Gallery>
    </Wrapper>
  )
}
