import React from 'react'
import styled from 'styled-components'
import Log from '../images/photo-gallery.png'

const MainWrapper = styled.div`
  background-color: #001d3d;
  margin-top: 30px;
  padding-bottom: 5px;
  margin: 0;
`

const Wrapper = styled.div`
  display: flex;
  justify-content: space-between;
  background-color: #023047;
  padding: 0 !important;
  color: #fff;
  width: 100%;
`
const LeftFooter = styled.div`
  margin-left: 60px;
`
const CenterFooter = styled.div``
const RightFooter = styled.div`
  margin-right: 60px;
  font-family: 'Nunito';
`
const Logo = styled.p`
  font-size: 25px;

  img {
    height: 100%;
    width: 60px;
    object-fit: contain;
  }
`
const Text = styled.p`
  font-family: 'Nunito';
  color: #f2e8cf;
  font-size: 22px;
  font-family: 'Nunito';
`
const MiniText = styled.p`
  font-family: 'Nunito';
  font-size: 16px;
  color: #06d6a0;
`
const Header1 = styled.h3`
  font-family: 'Nunito';
  color: #06d6a0;
`
const Links = styled.div`
  display: grid;
  grid-template-columns: repeat(1, auto);
  a {
    margin-bottom: 10px;
    text-decoration: none;
    color: #fff;
    font-family: 'Nunito';
    cursor: pointer;
    &:hover {
      color: #f72585;
    }
  }
`
const Header2 = styled.h3`
  font-family: 'Nunito';
  color: #06d6a0;
`
const Contact = styled.h3``
const Email = styled.h3``
const CopyRight = styled.p`
  font-size: 16px;
  color: #6c757d;
  text-align: center;
  font-family: 'Nunito';
`

export default function footer() {
  return (
    <MainWrapper>
      <Wrapper>
        <LeftFooter>
          <Logo>
            <img src={Log} alt="logo" />
          </Logo>
          <Text>Upload Your Best Memory Here</Text>
          <MiniText>Take me back to those good old days again</MiniText>
        </LeftFooter>
        <CenterFooter>
          <Header1>Useful links</Header1>
          <Links>
            <a href="#">Home</a>
            <a href="#">category</a>
            <a href="#">SignIn</a>
          </Links>
        </CenterFooter>
        <RightFooter>
          <Header2>Contact</Header2>
          <Contact>+254743126202</Contact>
          <Email>peterkanyora341@gmail.com</Email>
        </RightFooter>
      </Wrapper>
      <CopyRight>&copy;{new Date().getFullYear()} Copyright: PMKanyora</CopyRight>
    </MainWrapper>
  )
}
