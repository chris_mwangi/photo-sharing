/* eslint-disable no-unused-vars */
/* eslint-disable prettier/prettier */
import React from 'react'
import styled from 'styled-components'
import Log from '../images/photo-gallery.png'
import { googleLogout } from '@react-oauth/google'
import { GoogleLogin } from '@react-oauth/google'
import { createOrGetUser } from '../utils/auth'
import useAuthStore from '../store/auth'

const Wrapper = styled.div`
  color: #1d3557;
  display: flex;
  align-items: center;
  justify-content: space-between;
  font-family: 'Nunito';
  background-color: ${({ theme }) => theme.colors.white};
  height: 62px;
  border-bottom: 1px solid #eee;
  position: fixed;
  top: 0;
  left: 0;
  width: 100%;
  z-index: 100;
`
const Paragraph = styled.div`
  display: flex;
  column-gap: 23px;
  margin-right: 4rem;
`
const RoutePath = styled.p`
  font-family: 'Nunito';
  font-weight: 500;
  color: #1d3557;
  cursor: pointer;
  &:hover {
    color: ${({ theme }) => theme.colors.primary} !important;
  }
`
const Logo = styled.div`
  margin-left: 4rem;
  img {
    height: 100%;
    width: 60px;
    object-fit: contain;
  }
`
const Auth = styled.div`
  display: flex;
  align-items: center;
`
const UserImg = styled.div`
  border-radius: 50%;
  overflow: none;
  position: relative;
  display: flex;
  align-items: center;
  cursor: pointer;
  img {
    height: 2rem;
    border-radius: 50%;
    width: 2rem;
    object-fit: contain;
  }
`

const header = () => {
  const { addUser, userProfile, removeUser } = useAuthStore()

  return (
    <Wrapper>
      <Logo>
        <img src={Log} alt="logo" />
      </Logo>
      <Paragraph>
        {/* {
        Links.map((link) => (
            <a href="#" style={{ textDecoration: 'none', fontWeight: 'bold' }}>
            {link.name} </a>
          ))
        } */}
        <RoutePath href="#">Home</RoutePath>
        <RoutePath href="#">Category</RoutePath>
        <RoutePath href="#">Popular Photo</RoutePath>
        {/*  <RoutePath onClick={() => login()}>SignIn</RoutePath> */}
        <Auth>
          {!userProfile ? (
            <GoogleLogin
              onSuccess={(credentialResponse) => {
                createOrGetUser(credentialResponse, addUser)
              }}
              onError={() => {
                console.log('Login Failed')
              }}
            />
          ) : (
            <UserImg
              onClick={() => {
                googleLogout()
                removeUser()
              }}>
              <img src={userProfile.image} alt="user" />
            </UserImg>
          )}
        </Auth>
      </Paragraph>
    </Wrapper>
  )
}

export default header
