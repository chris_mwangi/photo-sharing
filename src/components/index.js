import React from 'react'
import styled from 'styled-components'
import Header from './header'
import Category from './category'
import Footer from './footer'
import Banner from './banner'

const Wrapper = styled.div`
  /*   &::-webkit-scrollbar {
    width: 0px;
  }
  &::-webkit-scrollbar-track {
    height: '100%';
    background: transparent;
    width: 0px;
  } */
`

function Landing() {
  return (
    <Wrapper>
      <Banner />
      <Header />
      <Category />
      {/* put components here start with  hreader */}
      <Footer />
    </Wrapper>
  )
}

export default Landing
