import React from 'react'
import styled from 'styled-components'

const Wrapper = styled.div`
  margin-top: 100px;
`
const Title = styled.h2`
  margin-left: 100px;
  color: #240046;
  font-size: 30px;
`
const Topics = styled.p`
  display: flex;
  width: 90%;
  margin-left: 80px;
  align-items: center;
  justify-content: space-between;
  cursor: pointer;
  img {
    height: 55px;
    width: 55px;
    border-radius: 50%;
  }
`
const Sub = styled.div`
  h3 {
    text-align: center;
    color: #1a659e;
  }
`

const Category = () => {
  return (
    <Wrapper>
      <Title>Best Categories</Title>
      <Topics>
        <Sub>
          <img src="./Image/nature.jpg" alt="nature" />
          <h3>Nature</h3>
        </Sub>
        <Sub>
          <img src="./Image/sports.jpg" alt="sports" />
          <h3>Sports</h3>
        </Sub>
        <Sub>
          <img src="./Image/art.jpg" alt="art" />
          <h3>Art</h3>
        </Sub>
        <Sub>
          <img src="./Image/space.jpg" alt="space" />
          <h3>Space</h3>
        </Sub>
        <Sub>
          <img src="./Image/animal.jpg" alt="animal" />
          <h3>Animals</h3>
        </Sub>
        <Sub>
          <img src="./Image/entertainment.jpg" alt="entertainment" />
          <h3>Entertainment</h3>
        </Sub>
        <Sub>
          <img src="./Image/meme.jpg" alt="meme" />
          <h3>Memes</h3>
        </Sub>
        <Sub>
          <img src="./Image/news.jpg" alt="news" />
          <h3>News</h3>
        </Sub>
      </Topics>
    </Wrapper>
  )
}

export default Category
