/* eslint-disable prettier/prettier */
/* eslint-disable no-unused-vars */
/* eslint-disable no-undef */
import React from 'react'
import { useEffect, useState } from 'react'
import jwt_decode from 'jwt-decode'

function App() {
  const [user, setUser] = useState({})

  const handleCallbackResponse = (response) => {
    console.log('Encoded JWT Id token:' + response.credential)
    let userObject = jwt_decode(response.credential)
    console.log(userObject)
    setUser(userObject)
    document.getElementById('signInDiv').hidden = true
  }

  const handleSignOut = (event) => {
    setUser({})
    document.getElementById('signInDiv').hidden = false
  }
  //google global
  useEffect(() => {
    google.accounts.id.initialize({
      client_id: '1047414415949-gl8u3c5dcu02eifhev5ncbhpnv28olbe.apps.googleusercontent.com',
      callback: handleCallbackResponse
    })

    google.accounts.id.renderButton(document.getElementById('signInDiv'), {
      theme: 'outline',
      size: 'large'
    })
  }, [])
  //If we have no user: show sign in button
  //If we have a user: show sign out button

  return (
    <div className="App" style={{ textAlign: 'center' }}>
      <div id="signInDiv" style={{ textAlign: 'center' }}></div>
      {Object.keys(user).length != 0 && (
        <button type="button" onClick={(e) => handleSignOut()}>
          SignOut
        </button>
      )}
      {user && (
        <div>
          <img src={user.picture}></img>
          <h3>{user.name}</h3>
        </div>
      )}
    </div>
  )
}

export default App
